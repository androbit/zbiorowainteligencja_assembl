      [paste.app_factory]
      main = assembl:main
      maintenance = assembl.maintenance:main
      [console_scripts]
      assembl-db-manage = assembl.scripts.db_manage:main
      assembl-ini-files = assembl.scripts.ini_files:main
      assembl-imap-test = assembl.scripts.imap_test:main
      assembl-add-user  = assembl.scripts.add_user:main
      assembl-pypsql  = assembl.scripts.pypsql:main
      assembl-pshell  = assembl.scripts.pshell:main
      assembl-pserve   = assembl.scripts.pserve:main
      assembl-reindex-all-contents  = assembl.scripts.reindex_all_contents:main
      