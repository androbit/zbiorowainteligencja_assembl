assembl.tests.fixtures package
==============================

Submodules
----------

.. toctree::

   assembl.tests.fixtures.auth
   assembl.tests.fixtures.base
   assembl.tests.fixtures.creativity_session
   assembl.tests.fixtures.discussion
   assembl.tests.fixtures.graphql
   assembl.tests.fixtures.idea_content_links
   assembl.tests.fixtures.ideas
   assembl.tests.fixtures.langstring
   assembl.tests.fixtures.locale
   assembl.tests.fixtures.mailbox
   assembl.tests.fixtures.posts
   assembl.tests.fixtures.preferences
   assembl.tests.fixtures.user
   assembl.tests.fixtures.user_language_preference

Module contents
---------------

.. automodule:: assembl.tests.fixtures
    :members:
    :show-inheritance:
