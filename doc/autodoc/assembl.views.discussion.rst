assembl.views.discussion package
================================

Submodules
----------

.. toctree::

   assembl.views.discussion.views

Module contents
---------------

.. automodule:: assembl.views.discussion
    :members:
    :show-inheritance:
