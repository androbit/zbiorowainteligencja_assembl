assembl.nlp package
===================

Submodules
----------

.. toctree::

   assembl.nlp.clusters
   assembl.nlp.indexedcorpus
   assembl.nlp.optics
   assembl.nlp.translation_service
   assembl.nlp.wordcounter

Module contents
---------------

.. automodule:: assembl.nlp
    :members:
    :show-inheritance:
