assembl.graphql package
=======================

Submodules
----------

.. toctree::

   assembl.graphql.schema
   assembl.graphql.types
   assembl.graphql.view

Module contents
---------------

.. automodule:: assembl.graphql
    :members:
    :show-inheritance:
