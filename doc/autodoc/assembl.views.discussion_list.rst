assembl.views.discussion_list package
=====================================

Submodules
----------

.. toctree::

   assembl.views.discussion_list.views

Module contents
---------------

.. automodule:: assembl.views.discussion_list
    :members:
    :show-inheritance:
