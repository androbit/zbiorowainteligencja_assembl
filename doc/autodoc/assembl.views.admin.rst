assembl.views.admin package
===========================

Submodules
----------

.. toctree::

   assembl.views.admin.views

Module contents
---------------

.. automodule:: assembl.views.admin
    :members:
    :show-inheritance:
