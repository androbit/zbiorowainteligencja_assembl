assembl.scripts package
=======================

Submodules
----------

.. toctree::

   assembl.scripts.add_user
   assembl.scripts.clean_avatars
   assembl.scripts.clone_database
   assembl.scripts.clone_discussion
   assembl.scripts.db_manage
   assembl.scripts.deduplicate_subscriptions
   assembl.scripts.ini_files
   assembl.scripts.link_cleanup
   assembl.scripts.list_css_icons
   assembl.scripts.make_er_diagram
   assembl.scripts.po2json
   assembl.scripts.pserve
   assembl.scripts.pshell
   assembl.scripts.pypsql
   assembl.scripts.rebuild_tables
   assembl.scripts.reindex_all_contents

Module contents
---------------

.. automodule:: assembl.scripts
    :members:
    :show-inheritance:
