assembl.tasks package
=====================

Submodules
----------

.. toctree::

   assembl.tasks.celery
   assembl.tasks.changes_router
   assembl.tasks.create_vmm_source
   assembl.tasks.email_discussion_creator
   assembl.tasks.imap
   assembl.tasks.imaplib2_source_reader
   assembl.tasks.notification_dispatch
   assembl.tasks.notify
   assembl.tasks.piwik
   assembl.tasks.source_reader
   assembl.tasks.threaded_model_watcher
   assembl.tasks.translate

Module contents
---------------

.. automodule:: assembl.tasks
    :members:
    :show-inheritance:
