assembl.views.search package
============================

Submodules
----------

.. toctree::

   assembl.views.search.views

Module contents
---------------

.. automodule:: assembl.views.search
    :members:
    :show-inheritance:
