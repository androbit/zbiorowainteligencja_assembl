

preferencesView
===============

.. currentmodule:: app.views


.. js:class:: BasePreferenceView ()

    Abstract class for preference views

    
    



.. js:class:: BoolPreferenceView ()

    View to set a Boolean preference

    :extends: :js:class:`app.views.preferencesView.BasePreferenceView`
    



.. js:class:: DictPreferencesItemView ()

    A single preference item in a DictPreferenceView

    :extends: :js:class:`app.views.preferencesView.PreferencesItemView`
    



.. js:class:: DictPreferenceView ()

    A single preference which is a dict

    :extends: :js:class:`app.views.preferencesView.ListPreferenceView`
    



.. js:class:: DictSubviewCollectionView ()

    The collection view for the items in a preference-as-dict

    
    



.. js:class:: DiscussionPreferenceCollectionSubset ()

    The subset of preferences which allow a per-discussion override

    :extends: :js:class:`app.views.preferencesView.PreferenceCollectionSubset`
    



.. js:class:: DiscussionPreferencesView ()

    The discussion preferences window

    :extends: :js:class:`app.views.preferencesView.PreferencesView`
    



.. js:class:: DomainPreferenceView ()

    View to set a domain (DNS name) value preference

    :extends: :js:class:`app.views.preferencesView.StringPreferenceView`
    



.. js:class:: EmailPreferenceView ()

    View to set an email value preference

    :extends: :js:class:`app.views.preferencesView.StringPreferenceView`
    



.. js:class:: GlobalPreferenceCollectionSubset ()

    The subset of preferences which allow a per-discussion override

    :extends: :js:class:`app.views.preferencesView.PreferenceCollectionSubset`
    



.. js:class:: GlobalPreferencesView ()

    The preferences window for global (instance-level) preferences

    :extends: :js:class:`app.views.preferencesView.PreferencesView`
    



.. js:class:: IntPreferenceView ()

    View to set an integer value preference

    :extends: :js:class:`app.views.preferencesView.StringPreferenceView`
    



.. js:class:: JsonPreferenceView ()

    View to set a JSON value preference

    :extends: :js:class:`app.views.preferencesView.TextPreferenceView`
    



.. js:class:: ListPreferencesItemView ()

    A single preference item in a ListPreferenceView

    :extends: :js:class:`app.views.preferencesView.PreferencesItemView`
    



.. js:class:: ListPreferenceView ()

    A single preference which is a list

    :extends: :js:class:`app.views.preferencesView.BasePreferenceView`
    



.. js:class:: ListSubviewCollectionView ()

    The collection view for the items in a preference-as-list

    
    



.. js:class:: LocalePreferenceView ()

    View to set a locale value preference (chosen from the set of locales)

    :extends: :js:class:`app.views.preferencesView.ScalarPreferenceView`
    



.. js:class:: PermissionPreferenceView ()

    View to set a permission value preference (chosen from the set of permissions)

    :extends: :js:class:`app.views.preferencesView.ScalarPreferenceView`
    



.. js:class:: PreferenceCollectionSubset ()

    Which preferences will we show?

    
    



.. js:class:: PreferencesCollectionView ()

    The list of all preferences

    
    



.. js:class:: PreferencesItemView ()

    A single preference item

    
    



.. js:class:: PreferencesView ()

    The preferences window

    
    



.. js:class:: RolePreferenceView ()

    View to set a role value preference (chosen from the set of roles)

    :extends: :js:class:`app.views.preferencesView.ScalarPreferenceView`
    



.. js:class:: ScalarPreferenceView ()

    View to set a scalar value preference (chosen from a set)

    :extends: :js:class:`app.views.preferencesView.BasePreferenceView`
    



.. js:class:: StringPreferenceView ()

    View to set a string value preference

    :extends: :js:class:`app.views.preferencesView.BasePreferenceView`
    



.. js:class:: TextPreferenceView ()

    View to set a text preference

    :extends: :js:class:`app.views.preferencesView.BasePreferenceView`
    



.. js:class:: UrlPreferenceView ()

    View to set a URL value preference

    :extends: :js:class:`app.views.preferencesView.StringPreferenceView`
    



.. js:class:: UserPreferenceCollectionSubset ()

    The subset of preferences which allow a per-user override

    :extends: :js:class:`app.views.preferencesView.PreferenceCollectionSubset`
    



.. js:class:: UserPreferencesView ()

    The user preferences window

    :extends: :js:class:`app.views.preferencesView.PreferencesView`
    




