

messageList
===========

.. currentmodule:: app.views


.. js:class:: BaseMessageList ()

    :extends: :js:class:`app.views.assemblPanel.AssemblPanel, app.views.baseMessageList.BaseMessageListMixin`
    



.. js:class:: MessageList ()

    :extends: :js:class:`app.views.messageList.BaseMessageList`
    
    .. js:function:: showPendingMessages(nbMessage)
    
        Shows the number of pending messages added through the socket
    
        :param number nbMessage: The number of new messages




