admin
====================================================

.. toctree::
    :maxdepth: 2

    adminDiscussion
    adminDiscussionSettings
    adminMessageColumns
    adminNavigationMenu
    adminNotificationSubscriptions
    adminPartners
    adminTimelineEvents
    generalSource
    simpleLangStringEdit
    sourceEditViews