

baseMessageList
===============

.. currentmodule:: app.views


.. js:class:: BaseMessageListMixin ()

    A mixin for message list behaviour. Logic here, UI in subclasses.
    Note: implementing the mixin as a class->class functor is inspired by
    http://justinfagnani.com/2015/12/21/real-mixins-with-javascript-classes/

    
    




