

groupContainer
==============

.. currentmodule:: app.views.groups


.. js:class:: groupContainer ()

    Manages all the groups in the interface, essentially the GroupSpec.Collection
    Normally referenced with Assembl.groupContainer

    
    




