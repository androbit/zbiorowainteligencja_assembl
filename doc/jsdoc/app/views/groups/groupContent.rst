

groupContent
============

.. currentmodule:: app.views.groups


.. js:class:: groupContent ()

    Represents the entire content of a single panel group

    
    




