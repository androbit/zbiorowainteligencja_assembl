utils
====================================================

.. toctree::
    :maxdepth: 2

    browser-detect
    cookiesManager
    genieEffect
    growl
    i18n
    panelSpecTypes
    permissions
    roles
    scrollUtils
    socket
    tourManager
    types
    tours/__index__