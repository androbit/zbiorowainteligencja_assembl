

ideaContentLink
===============

The link between an idea and a message.



.. currentmodule:: app.models

.. js:function:: IdeaContentLinkTypeRank()

.. js:class:: Collection ()

    Idea content link collection
    This Collection is NOT created from an API call, like most other models, collections.
    It will be created from an array that will be passed from the message model.

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.ideaContentLink.Collection.initialize
    
        :type: string
    .. js:attribute:: app.models.ideaContentLink.Collection.url
    
        :type: string
    .. js:function:: comparator(one, two)
    
        Firstly, sort based on direct vs indirect, then sort based on types. If the types match, sort in ascending order.
    
        :param Object one:
        :param Object two:
    .. js:function:: constructor()
    .. js:function:: getIdeaNamesPromise()
    
        The string of short names of the ideas that a message is associated to. Note: It does not contain those that the user clipboarded.
    
        :rtype: Array



.. js:class:: IdeaContentLinkModel ()

    Idea content link model
    Frontend model for :py:class:`assembl.models.idea_content_link.IdeaContentLink`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:attribute:: app.models.ideaContentLink.IdeaContentLinkModel.urlRoot
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: getCreationDate()
    
        Backend sends a created field instead of a creation_date
    
        :rtype: String
    .. js:function:: getLinkCreatorModelPromise()
    
        Returns the link creator model promise
    
        :rtype: Promise
    .. js:function:: getMessageStructurePromise()
    
        Returns idea model promise
    
        :rtype: Promise
    .. js:function:: getMessageStructurePromise()
    
        Returns message structure model promise
    
        :rtype: Promise
    .. js:function:: getPostCreatorModelPromise()
    
        Returns the post creator model promise
    
        :rtype: Promise
    .. js:function:: isDirect()
    
        Helper function for the comparator, might not work
    
        :rtype: Boolean




