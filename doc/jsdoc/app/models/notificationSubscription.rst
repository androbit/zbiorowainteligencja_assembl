

notificationSubscription
========================

A user's subscription to being notified of certain situations



.. currentmodule:: app.models


.. js:class:: notificationsSubscriptionCollection ()

    Notifications subscription collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: notificationsSubscriptionModel ()

    Notification subscription model
    Frontend model for :py:class:`assembl.models.notification.NotificationSubscription`

    :extends: :js:class:`app.models.base.BaseModel`
    




