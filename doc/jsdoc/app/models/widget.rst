

widget
======

.. currentmodule:: app.models


.. js:class:: CreativitySessionWidgetModel` ()

    Represents a Creativity Session Widget
    Frontend model for :py:class:`assembl.models.widgets.CreativitySessionWidget`

    :extends: :js:class:`app.models.widget.WidgetModel`
    



.. js:class:: IdeaVoteModel ()

    This model represents a single vote of a user on an idea.
    Frontend model for :py:class:`assembl.models.votes.AbstractIdeaVote`

    :extends: :js:class:`app.models.base.BaseModel`
    



.. js:class:: MultiCriterionVotingWidgetModel` ()

    This represents a multi-criterion voting widget
    Frontend model for :py:class:`assembl.models.widgets.MultiCriterionVotingWidgetModel`

    :extends: :js:class:`app.models.widget.VotingWidgetModel`
    



.. js:class:: TokenCategorySpecificationModel ()

    The specifications describing a token category
    Frontend model for :py:class:`assembl.models.votes.TokenCategorySpecification`

    :extends: :js:class:`app.models.base.BaseModel`
    



.. js:class:: TokenIdeaVoteModel ()

    This model represents a single token vote of a user on an idea.
    Frontend model for :py:class:`assembl.models.votes.TokenIdeaVote`

    :extends: :js:class:`app.models.widget.IdeaVoteModel`
    



.. js:class:: TokenVoteSpecificationModel ()

    The specifications describing how a vote should happen
    Frontend model for :py:class:`assembl.models.votes.TokenVoteSpecification`

    :extends: :js:class:`app.models.base.BaseModel`
    



.. js:class:: TokenVotingWidgetModel` ()

    This represents a token voting voting widget
    Frontend model for :py:class:`assembl.models.widgets.TokenVotingWidgetModel`

    :extends: :js:class:`app.models.widget.VotingWidgetModel`
    



.. js:class:: VotingWidgetModel` ()

    This represents a voting widget
    Frontend model for :py:class:`assembl.models.widgets.VotingWidget`

    :extends: :js:class:`app.models.widget.WidgetModel`
    



.. js:class:: WidgetModel` ()

    This represents a widget, a set of bundled functionality.
    Frontend model for :py:class:`assembl.models.widgets.Widget`

    :extends: :js:class:`app.models.base.BaseModel`
    



.. js:class:: WidgetSubset ()

    A subset of the widgets relevant to a widget context

    
    




