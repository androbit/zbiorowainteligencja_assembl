

idea
====

Represent an idea



.. currentmodule:: app.models


.. js:class:: IdeaCollection ()

    Ideas collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.idea.IdeaCollection.url
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: getOrderForNewRootIdea()
    
        Returns the order number for a new root idea
    
        :rtype: Number
    .. js:function:: getRootIdea()
    
        Returns the root idea
    
        :rtype: Object
    .. js:function:: updateRootIdeasOrder()
    
        Updates the order in the idea list
    .. js:function:: visitBreadthFirst(idea_links, visitor, origin_id, ancestry)
    
        :param Object idea_links: The collection of idea_links to navigate
        :param Object visitor: Visitor function
        :param String origin_id: the id of the root
        :param Object ancestry: Internal recursion parameter, do not set or use
    .. js:function:: visitDepthFirst(idea_links, visitor, origin_id, ancestry)
    
        :param Object idea_links: The collection of idea_links to navigate
        :param Object visitor: Visitor function
        :param String origin_id: the id of the root
        :param Object ancestry: Internal recursion parameter, do not set or use



.. js:class:: IdeaModel ()

    Idea model
    Frontend model for :py:class:`assembl.models.idea.Idea`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:attribute:: app.models.idea.IdeaModel.urlRoot
    
        :type: string
    .. js:function:: addChild(idea)
    
        Adds an idea as child
    
        :param Idea idea:
    .. js:function:: addSegment(segment)
    
        Adds a segment
    
        :param Object segment:
    .. js:function:: addSegmentAsChild(segment,)
    
        Creates a new instance of a segment as a child within the collection and returns the newly created idea.
    
        :param Segment segment,: possibly unsaved.
        :rtype: Object
    .. js:function:: addSiblingAbove(idea)
    
        Adds an idea as sibling above
    
        :param Idea idea:
    .. js:function:: addSiblingBelow(idea)
    
        Adds an idea as sibling below
    
        :param Idea idea:
    .. js:function:: adjust_num_read_posts()
    
        Set the number of read posts
    .. js:function:: constructor()
    .. js:function:: getAncestry()
    
        Returns an array of Idea models in order of ancestry From current idea -> parent idea, including the current idea itself.
    
        :rtype: Array
    .. js:function:: getApplicableAnnouncementPromise()
    
        Returns a promise for the announcement to be displayed in the message-list, if any
    
        :rtype: Promise
    .. js:function:: getChildren()
    
        Return all children's idea
    
        :rtype: Array
    .. js:function:: getDefinitionDisplayText()
    
        Returns the display text for a idea definition.
        Will return the first non-empty from: definition, longTitle, i18n.gettext('Add a definition for this idea')
    
        :rtype: string
    .. js:function:: getExtractsPromise()
    
        Return a promise for all Extracts models for this idea
    
        :rtype: Promise
    .. js:function:: getLevel()
    
        Returns the indentantion level
    
        :rtype: number
    .. js:function:: getLongTitleDisplayText()
    
        Returns the display text for a idea synthesis expression. Will return the first non-empty from: longTitle, shortTitle, i18n.gettext('Add and expression for the next synthesis')
    
        :rtype: string
    .. js:function:: getOrderForNewChild()
    
        Returns the order number for a new child
    
        :rtype: number
    .. js:function:: getParent()
    
        Return the parent idea
    
        :return: or undefined
        :rtype: Object
    .. js:function:: getShortTitleDisplayText()
    
        HTML Striping if necessary is the responsability of the caller.
    
        :return: The short Title to be displayed
        :rtype: string
    .. js:function:: initialize()
    .. js:function:: isDescendantOf(idea)
    
        Return if the idea is descendant of the given idea
    
        :param Object idea:
        :rtype: boolean
    .. js:function:: isRootIdea()
    
        Returns true if the current idea is the root idea
    
        :rtype: boolean
    .. js:function:: parse()
    
        Returns the attributes hash to be set on the model
    .. js:function:: set(key, val, options)
    
        Set a hash of attributes on the model.
    
        :param String key:
        :param val val:
        :param Object options:
        :rtype: Object
    .. js:function:: updateChildrenOrder()
    
        Updates the order in all children
    .. js:function:: validate()
    
        Validate the model attributes




