

partners
========

A partner organization, to be displayed in front page



.. currentmodule:: app.models


.. js:class:: PartnerOrganizationCollection ()

    Partner collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: PartnerOrganizationModel ()

    Partner model
    Frontend model for :py:class:`assembl.models.auth.PartnerOrganization`

    :extends: :js:class:`app.models.base.BaseModel`
    




