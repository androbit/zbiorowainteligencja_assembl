

ideaMessageColumn
=================

Description of the columns of classified messages under an idea



.. currentmodule:: app.models


.. js:class:: IdeaLinkCollection ()

    The collection of categories of classified messages under an idea

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: IdeaMessageColumnModel ()

    A category of classified messages under an idea
    Frontend model for :py:class:`assembl.models.idea_msg_column.IdeaMessageColumn`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: initialize()




