

social
======

An access token for facebook



.. currentmodule:: app.models


.. js:class:: FacebookAccessToken ()

    Facebook access token collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: FacebookAccessToken ()

    Facebook access token model
    Frontend model for :py:class:`assembl.models.facebook_integration.FacebookAccessToken`

    :extends: :js:class:`app.models.base.BaseModel`
    




