

ideaLink
========

The link between two ideas



.. currentmodule:: app.models


.. js:class:: IdeaLinkCollection ()

    Idea link collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.ideaLink.IdeaLinkCollection.url
    
        :type: string



.. js:class:: IdeaLinkModel ()

    Idea link model
    Frontend model for :py:class:`assembl.models.idea.IdeaLink`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: initialize()
    .. js:function:: validate()
    
        Validate the model attributes




