

synthesis
=========

The collection of idea snapshots in a synthesis



.. currentmodule:: app.models


.. js:class:: SynthesisCollection ()

    Synthesis collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: SynthesisIdeaCollection ()

    Synthesis ideas collection

    :extends: :js:class:`app.models.idea.IdeaCollection`
    



.. js:class:: SynthesisModel ()

    Synthesis model
    Frontend model for :py:class:`assembl.models.idea_graph_view.Synthesis`

    :extends: :js:class:`app.models.base.BaseModel`
    




