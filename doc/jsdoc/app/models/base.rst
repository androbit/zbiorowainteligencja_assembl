

base
====

.. currentmodule:: app.models


.. js:class:: BaseCollection ()

    BaseCollection which should be used by ALL collections

    
    



.. js:class:: BaseModel ()

    BaseModel which should be used by ALL models

    :extends: :js:class:`Backbone.Model`
    
    .. js:attribute:: app.models.base.BaseModel.idAttribute
    
        Overwriting the idAttribute
    
        :type: uri
    .. js:function:: getId()
    
        Alias for `.get('id') || .get('@id') || .cid`
    
        :rtype: string
    .. js:function:: getNumericId()
    
        Get the numeric id from the id string
    
        :rtype: number
        
    
        .. code-block:: js
        
            finds '30' if the id is 'local:ModelName/30'



.. js:class:: RelationsCollection ()

    Collection of relationships to objects which "exist" in another
    BaseCollection, but the relationships are not materialized.
    So add/remove should not create/delete object, but relation.

    
    




