

flipSwitchButton
================

Button to switch to less/more filter options



.. currentmodule:: app.models


.. js:class:: FlipSwitchButtonModel ()

    Flip switch button model

    
    




