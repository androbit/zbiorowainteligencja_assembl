

groupState
==========

Represents the state of a panel group (current idea, selected navigation, minimised states, etc.)



.. currentmodule:: app.models


.. js:class:: GroupStateModel ()

    Group state model

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: constructor()
    .. js:function:: toJSON()
    
        Return a copy of the model's attributes for JSON stringification.
    .. js:function:: validate()
    
        Validate the model attributes and returns an error message if the model is invalid and undefined if the model is valid



.. js:class:: GroupStates ()

    Group states collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:function:: constructor()
    .. js:function:: validate()
    
        Validate the model attributes




