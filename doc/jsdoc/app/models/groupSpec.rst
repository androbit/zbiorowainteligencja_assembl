

groupSpec
=========

Represents an independent group of panels in the interface. When added, the matching views (groupContainerView) will be instanciated



.. currentmodule:: app.models


.. js:class:: GroupSpecModel ()

    Group specifications model

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: addPanel(options, position)
    
        Add panel specs to model
    
        :param Object options:
        :param Int position:
    .. js:function:: constructor()
    .. js:function:: defaults()
    
        Set panelSpec and groupState in model attributes
    
        :rtype: Object
    .. js:function:: ensurePanelsAt(list_of_options, position)
    
        Find or create panels at a given position
        Note that despite the name, this function does NOT check that if the panel exists, it exists at the right position.
        It ONLY check that the panel exists
    
        :param Array list_of_options: PanelType or array of PanelType
        :param Int position: int order of first panel listed in sequence of panels
    .. js:function:: findNavigationPanelSpec()
    
        Returns the part of the groupSpec that contains the navigation panel (if any).
        That is, any panel in the first position that has the capacity to alter the global group state
    
        :rtype: Object
    .. js:function:: findNavigationSidebarPanelSpec()
    
        Returns the part of the groupSpec that contains the simple interface navigation panel (if any)
    
        :rtype: Object
    .. js:function:: getPanelSpecByType(panelSpecType)
    
        Returns panel sepcs by type
    
        :param Object panelSpecType:
    .. js:function:: parse(model)
    
        :param Object model:
        :rtype: Object
    .. js:function:: removePanelByModel(aPanelSpec)
    
        Remove panel specs from model
    
        :param aPanelSpec aPanelSpec:
    .. js:function:: removePanels()
    
        Remove panel specs from model
    .. js:function:: validate()
    
        Validate the model attributes



.. js:class:: GroupSpecs ()

    Group specifications collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:function:: constructor()
    .. js:function:: validate()
    
        Validate the model attributes




