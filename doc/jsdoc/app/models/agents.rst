

agents
======

User profile and permissions (user or email author)



.. currentmodule:: app.models


.. js:class:: AgentCollection ()

    Agents collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.agents.AgentCollection.model
    
        The model
    
        :type: AgentModel
    .. js:attribute:: app.models.agents.AgentCollection.url
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: getById(id)
    
        Returns the user by his/her id, or return the unknown user
    
        :param Number id:
        :rtype: Object
    .. js:function:: getUnknownUser()
    
        Returns the unknown user object
    
        :rtype: Object



.. js:class:: AgentModel ()

    Agent model
    Frontend model for :py:class:`assembl.models.auth.AgentProfile`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:attribute:: app.models.agents.AgentModel.permissions
    
        The list with all user's permissions
        This is usefull only for the logged user.
    
        :type: Array.<string>
    .. js:attribute:: app.models.agents.AgentModel.urlRoot
    
        :type: string
    .. js:function:: can(permission)
    
        :param String permission:
        :rtype: Boolean
    .. js:function:: constructor()
    .. js:function:: fetchPermissions()
    
        Load permissions from database
    .. js:function:: fetchPermissionsFromScriptTag([id='permissions-json'])
    
        Load the permissions from script tag and populates `this.permissions`
    
        :param string id: The script tag id
    .. js:function:: getAvatarColor()
    
        Returns the avatar's color
    
        :rtype: string
    .. js:function:: getAvatarUrl([size=44])
    
        return the avatar's url
    
        :param number size: The avatar size
        :rtype: string
    .. js:function:: getRolesMissingMessageForPermission(permission, discussion, reroute_relative_url)
    
        A text message designed to replace X in the question "You cannot perform this operation because X"
    
        :param String permission:
        :param Number discussion:
        :param String reroute_relative_url:
        :rtype: string
    .. js:function:: hasPermission(permission)
    
        Checks if user has permission
    
        :param string permission: The permission name
        :return: True if the user has the given permission
        :rtype: boolean
    .. js:function:: isUnknownUser()
    
        Returns true if the user is an unknown user
    
        :rtype: Boolean
    .. js:function:: validate()
    
        Validate the model attributes




