

languagePreference
==================

A user's preference language



.. currentmodule:: app.models


.. js:class:: DisconnectedUserLanguagePreferenceCollection ()

    Language pseudo-preference-set for a disconnected user.

    :extends: :js:class:`app.models.base.languagePreference.LanguagePreferenceModel`
    



.. js:class:: LanguagePreferenceCollection ()

    Language Preference set of the user; there is a privacy setting which will only show an array of user preferences that are bound to the user

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.languagePreference.LanguagePreferenceCollection.url
    
        :type: string
    .. js:function:: comparator()
    
        Comparator sorts in ascending order
    .. js:function:: constructor()
    .. js:function:: getExplicitLanguages()
    
        Comparator sorts in ascending order
    .. js:function:: interface_comparator()
    
        Comparator sorts in ascending order, but explicit goes last.



.. js:class:: LanguagePreferenceModel ()

    A user's preference on how to handle a language: should it be translated, and to what?
    Frontend model for :py:class:`assembl.models.auth.UserLanguagePreference`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: constructor()
    .. js:function:: isLocale()
    .. js:function:: isTranslateTo()
    .. js:function:: setExplicitPromise()




