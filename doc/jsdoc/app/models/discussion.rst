

discussion
==========

Represents a discussion



.. currentmodule:: app.models


.. js:class:: discussionCollection ()

    Discussions collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.discussion.discussionCollection.url
    
        :type: string
    .. js:function:: constructor()



.. js:class:: discussionModel ()

    Discussion model
    Frontend model for :py:class:`assembl.models.discussion.Discussion`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:attribute:: app.models.discussion.discussionModel.url
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: getRolesForPermission(permission)
    
        Returns roles according to permission
    
        :param Object permission:
        :rtype: Array
    .. js:function:: getVisualizations()
    
        Get visualizations
    
        :rtype: BaseCollection
    .. js:function:: hasTranslationService()
    
        Checks if translation service is available
    
        :rtype: Boolean
    .. js:function:: validate()
    
        Validate the model attributes




