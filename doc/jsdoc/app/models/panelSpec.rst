

panelSpec
=========

Represents a panel in the interface. When added to the collection, the matching view (panelWrapper) will be instanciated



.. currentmodule:: app.models


.. js:class:: PanelSpecModel ()

    Panel specification model

    :extends: :js:class:`app.models.base.BaseModel`
    



.. js:class:: PanelSpecs ()

    Panel specifications collection

    :extends: :js:class:`app.models.base.BaseCollection`
    




