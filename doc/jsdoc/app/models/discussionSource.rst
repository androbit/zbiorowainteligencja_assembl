

discussionSource
================

Represents a discussion's messages from an external source.



.. currentmodule:: app.models


.. js:class:: sourceCollection ()

    Sources collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.discussionSource.sourceCollection.urlRoot
    
        :type: string
    .. js:function:: constructor()



.. js:class:: sourceModel ()

    Source model
    Frontend model for :py:class:`assembl.models.generic.ContentSource`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:attribute:: app.models.discussionSource.sourceModel.urlRoot
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: doReimport()
    
        Run import to backend server
    .. js:function:: doReprocess()
    
        Run process to backend server
    .. js:function:: validate()
    
        Validate the model attributes
    .. js:function:: validate()
    
        Validate the model attributes
    .. js:function:: validate()
    
        Validate the model attributes




