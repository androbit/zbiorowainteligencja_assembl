

accounts
========

.. currentmodule:: app.models


.. js:class:: Account ()

    A user's (email or social) account.
    Frontend model for :py:class:`assembl.models.auth.AbstractAgentAccount`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:attribute:: app.models.accounts.Account.urlRoot
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: isFacebookAccount()
    
        Returns true if the Account type is a Facebook account
    
        :rtype: Boolean
    .. js:function:: validate()
    
        Validate the model attributes



.. js:class:: Accounts ()

    Accounts collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.accounts.Accounts.url
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: getFacebookAccount()
    
        Returns Facebook account data
    
        :rtype: Object
    .. js:function:: hasFacebookAccount()
    
        Returns true if the Account type is a Facebook account
    
        :rtype: Boolean




