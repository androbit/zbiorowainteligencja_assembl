

userCustomData
==============

Custom key-value storage bound to a user and a namespace



.. currentmodule:: app.models


.. js:class:: UserCustomDataModel ()

    User custom data model
    Frontend model for :py:class:`assembl.models.user_key_values.DiscussionPerUserNamespacedKeyValue`

    :extends: :js:class:`Backbone.Model`
    




