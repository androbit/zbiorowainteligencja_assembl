

segment
=======

A segment of text extracted from a message. Can be associated to an idea, otherwise in clipboard.



.. currentmodule:: app.models


.. js:class:: SegmentCollection ()

    Segment collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: SegmentModel ()

    Segment model
    Frontend model for :py:class:`assembl.models.idea_content_link.Extract`

    :extends: :js:class:`app.models.base.BaseModel`
    




