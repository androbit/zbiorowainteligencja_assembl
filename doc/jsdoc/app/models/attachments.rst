

attachments
===========

Represents the link between an object (ex: Message, Idea) and a remote (url) or eventually local document attached to it.



.. currentmodule:: app.models


.. js:class:: AttachmentCollection ()

    Attachements collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:function:: comparator()
    
        Compares dates between 2 documents
    
        :rtype: Number
    .. js:function:: constructor()
    .. js:function:: destroy(models, options)
    
        Helper method to destroy the models in a collection
    
        :param Array|Backbone.Model models: Model or Array of models
        :param Object options: Options hash to send to every model when destroyed
        :return: if model was persisted, returns jqXhr else false
        :rtype: Promse
    .. js:function:: destroyAll(options)
    
        Destroy the collection
    
        :param Object options:
        :rtype: jqXHR
    .. js:function:: initialize()
    .. js:function:: save(models, options)
    
        Save models into database
    
        :param Object models:
        :param Object options:
        :rtype: Promise
    .. js:function:: saveAll(options)
    
        Save models into database
    
        :param Object options:
        :rtype: jqXHR
    .. js:function:: url()
    
        Returns api url dedicated to attachments
    
        :rtype: String



.. js:class:: AttachmentModel ()

    Attachement model
    Frontend model for :py:class:`assembl.models.attachment.Attachment`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: _saveMe(attrs, options)
    
        Set the id of the model attachment and save the model into database
    
        :param Object attrs:
        :param Object options:
        :rtype: jqXHR
    .. js:function:: constructor()
    .. js:function:: destroy()
    
        Destroys or removes the document from the server by using the Backbone.sync method which delegates the HTTP "delete" request.
    .. js:function:: getCreationDate()
    
        Returns the creation date of the attachment
    
        :rtype: Moment
    .. js:function:: getDocument()
    
        Returns the document attributes from the model
    
        :rtype: Object
    .. js:function:: isFailed()
    
        Returns if the model is unsavable.
    
        :rtype: Boolean
    .. js:function:: parse()
    
        Returns the model of the attachment according to its type (document or file)
    
        :rtype: BaseModel
    .. js:function:: save(attrs, options)
    
        Update
        =======
        
        The architecture to load attachments + documents has now changed.
        Documents are eagerly saved to the database upon creation.
        The AttachmentView is responsible for the lifecycle of the document model.
        As a result, the attachment model save should no longer do a two-step save process.
        It is only responsible for saving itself.
    
        :param Object attrs:
        :param Object options:
    .. js:function:: setFailed()
    
        Utility function. Makes the model unsavable.
    .. js:function:: sync(method, model, options)
    
        This method uses a switch according to a CRUD operation
        Default: Used to persist the state of the model to the server.
        Update: It does nothing
        Create: Set the type attribute to the model
    
        :param String method: update/create/read/delete methods
        :param BaseModel model: the model to be save
        :param Object options: It fires success or error message
    .. js:function:: toJSON(options)
    
        Return a copy of the model's attributes for JSON stringification.
        Override toJSON of the attachment model in order to ensure that backbone does NOT try to parse the an object that causes recursive read, as there is a message object which contains the attachment model.
    
        :param Object options:
        :rtype: Object
    .. js:function:: urlRoot()
    
        Returns api url dedicated to attachments
    
        :rtype: String
    .. js:function:: validate()
    
        Returns an error message if one of those attributes (objectAttachedToModel, document, idCreator) is missing
    
        :rtype: String




