

actions
=======

An action done by a user.



.. currentmodule:: app.models


.. js:class:: actionCollection ()

    Actions collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.actions.actionCollection.url
    
        :type: string
    .. js:function:: constructor()



.. js:class:: actionModel ()

    Action model
    Frontend model for :py:class:`assembl.models.action.Action`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:attribute:: app.models.actions.actionModel.urlRoot
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: validate()
    
        Validate the model attributes




