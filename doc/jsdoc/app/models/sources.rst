

sources
=======

An external source of imported messages



.. currentmodule:: app.models


.. js:class:: Source ()

    Source model
    Frontend model for :py:class:`assembl.models.generic.ContentSource` and :py:class:`assembl.models.post.PostSource`

    :extends: :js:class:`app.models.base.BaseModel`
    




