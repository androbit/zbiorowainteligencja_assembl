

roles
=====

A role that the user is granted in this discussion



.. currentmodule:: app.models


.. js:class:: roleCollection ()

    Roles collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: roleModel ()

    Role model
    Frontend model for :py:class:`assembl.models.auth.LocalUserRole`

    :extends: :js:class:`app.models.base.BaseModel`
    




