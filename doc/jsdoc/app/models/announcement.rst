

announcement
============

Represents an announcement, a mutable message-like object, with an author and a date



.. currentmodule:: app.models


.. js:class:: AnnouncementCollection ()

    Annoucements collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:function:: constructor()



.. js:class:: AnnouncementModel ()

    Annoucement model
    Frontend model for :py:class:`assembl.models.announcement.Announcement`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: constructor()
    .. js:function:: getCreatorPromise()
    
        Returns a promise for the post's creator
    
        :rtype: Promise
    .. js:function:: initialize()
    
        Returns an error message if the model format is invalid with th associated id
    
        :rtype: String
    .. js:function:: validate()
    
        Returns an error message if one of those attributes (idObjectAttachedTo, last_updated_by, creator) is missing
    
        :rtype: String




