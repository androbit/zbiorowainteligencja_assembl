

discussionPreference
====================

Discussion preferences



.. currentmodule:: app.models


.. js:class:: DiscussionIndividualPreferenceModel ()

    An individual preference value.
    We do not use Base.Model.extend(), because we want to keep Backbone's default behaviour with model urls.
    Generic case: preference value can be any json, not necessarily a dict.
    So put it in "value" attribute of this model.

    
    
    .. js:function:: constructor()
    .. js:function:: parse()
    .. js:function:: toJSON()



.. js:class:: DiscussionPreferenceCollection ()

    :extends: :js:class:`app.models.discussionPreference.PreferenceCollection`
    
    .. js:function:: constructor()
    .. js:function:: constructor()
    .. js:function:: constructor()
    .. js:function:: parse()
    .. js:function:: toJSON()



.. js:class:: DiscussionPreferenceCollection ()

    :extends: :js:class:`app.models.discussionPreference.PreferenceCollection`
    
    .. js:function:: constructor()
    .. js:function:: constructor()
    .. js:function:: constructor()
    .. js:function:: parse()
    .. js:function:: toJSON()



.. js:class:: DiscussionPreferenceDictionaryModel ()

    Subcase: pref is a dictionary, so we can use normal backbone

    
    
    .. js:function:: constructor()
    .. js:function:: url()



.. js:class:: DiscussionPreferenceSubCollection ()

    
    
    .. js:function:: constructor()



.. js:class:: PreferenceCollection ()

    
    



.. js:class:: UserPreferenceRawCollection ()

    :extends: :js:class:`app.models.discussionPreference.PreferenceCollection`
    
    .. js:function:: constructor()




