

documents
=========

Represents a file or document (a remote url or a blob)



.. currentmodule:: app.models


.. js:class:: DocumentCollection ()

    Documents collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:function:: constructor()



.. js:class:: DocumentModel ()

    Document model
    Frontend model for :py:class:`assembl.models.attachment.Document`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:attribute:: app.models.documents.DocumentModel.urlRoot
    
        :type: string
    .. js:function:: constructor()
    .. js:function:: isFileType()
    
        Checks if document type is a file
    
        :rtype: Boolean



.. js:class:: FileModel ()

    File model
    Frontend model for :py:class:`assembl.models.attachment.Document`

    :extends: :js:class:`app.models.documents.DocumentModel`
    
    .. js:function:: constructor()
    .. js:function:: isImageType()
    
        Returns a mime type
    
        :rtype: String
    .. js:function:: save()
    
        Save the model into database
        This model takes a fileAttribute of raw_data, which the backend will consume using a Multipart form header.
        In order to make the push a multi-part form header, must pass the option formData.
    
        :rtype: jqXHR




