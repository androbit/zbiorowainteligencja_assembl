

timeline
========

Description of the columns of classified messages under an idea



.. currentmodule:: app.models


.. js:class:: TimelineEventCollection ()

    The collection of categories of classified messages under an idea

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:attribute:: app.models.timeline.TimelineEventCollection.url
    
        :type: string



.. js:class:: TimelineEventModel ()

    A category of classified messages under an idea
    Frontend model for :py:class:`assembl.models.idea_msg_column.TimelineEvent`

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: initialize()




