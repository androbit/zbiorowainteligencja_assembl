

infobar
=======

Infobars for cookie and widget settings



.. currentmodule:: app.models


.. js:class:: InfobarModel ()

    Info bar model

    :extends: :js:class:`app.models.base.BaseModel`
    
    .. js:function:: constructor()



.. js:class:: InfobarsCollection ()

    Cookie and widget bars collection

    :extends: :js:class:`app.models.base.BaseCollection`
    
    .. js:function:: constructor()



.. js:class:: WidgetInfobarModel ()

    Widget bar model

    :extends: :js:class:`app.models.infobar.InfobarModel`
    
    .. js:function:: constructor()



.. js:class:: WidgetInfobarModel ()

    Cookie bar model

    :extends: :js:class:`app.models.infobar.InfobarModel`
    
    .. js:function:: constructor()




