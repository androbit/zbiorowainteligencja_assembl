

message
=======

A message (whether generated on assembl or imported from a ContentSource)



.. currentmodule:: app.models


.. js:class:: MessageCollection ()

    Messages collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: MessageModel ()

    Message model
    Frontend model for :py:class:`assembl.models.post.Post`

    :extends: :js:class:`app.models.base.BaseModel`
    




