

mock_server
===========

.. currentmodule:: app.tests

.. js:function:: ajaxMock(url, settings)

    transform an ajax call into the equivalent recorded API call fixture from
    :py:func:`assembl.tests.base.api_call_to_fname`

    :param string url: The url
    :param object settings: The settings


