

router
======

The application router.



.. currentmodule:: app


.. js:class:: Router ()

    The Router will forward existing URLs to various handlers according to those routes
    Keep in sync with :py:class:`assembl.lib.frontend_urls.FrontendUrls`

    :extends: :js:class:`Marionette.AppRouter`
    




