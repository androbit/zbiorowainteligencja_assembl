

collectionManager
=================

A singleton to manage data backend access with Ajax requests and Bluebird promises. Responsible for the data synchronization listening to a websocket and updating collections.



.. currentmodule:: app.common


.. js:class:: CollectionManager ()

    A singleton to manage lazy loading of server collections

    
    
    .. js:function:: _parseGroupStates(models, allIdeasCollection)
    
        :param Object models:
        :param Object allIdeasCollection:
        :return: models
        :rtype: Object
    .. js:function:: getAllAnnouncementCollectionPromise()
    
        Returns the collection of annoucements
    
        :rtype: BaseCollection
    .. js:function:: getAllExtractsCollectionPromise()
    
        Returns the collection of extracts
    
        :rtype: BaseCollection
    .. js:function:: getAllIdeaLinksCollectionPromise()
    
        Returns the collection of idea links
    
        :rtype: BaseCollection
    .. js:function:: getAllIdeasCollectionPromise()
    
        Returns the collection of ideas
    
        :rtype: BaseCollection
    .. js:function:: getAllMessageStructureCollectionPromise()
    
        Returns the collection of message structures
    
        :rtype: BaseCollection
    .. js:function:: getAllPartnerOrganizationCollectionPromise()
    
        Returns the collection of partners
    
        :rtype: BaseCollection
    .. js:function:: getAllPartnerOrganizationCollectionPromise()
    
        Returns the collection of partners
    
        :rtype: BaseCollection
    .. js:function:: getAllSynthesisCollectionPromise()
    
        Returns the collection of synthesis
    
        :rtype: BaseCollection
    .. js:function:: getAllUserAccountsPromise()
    
        Returns the collection of user accounts
    
        :rtype: BaseCollection
    .. js:function:: getAllUsersCollectionPromise()
    
        Returns the collection of users
        An exception, the collection is instanciated from json sent in the HTML of the frontend, not through an ajax request.
    
        :rtype: BaseCollection
    .. js:function:: getAllWidgetsPromise()
    
        Returns the collection of widgets
    
        :rtype: BaseCollection
    .. js:function:: getCollectionPromiseByType(item, item['@type')
    
        Returns the owning collection for the raw json of an object that 
        doesn't have a model yet.  Primarily used when receiving an object on
        the websocket
        
        Ex: A harvester changes the title of an idea.  The updated idea will be put
        on the websocket by the backend.  All frontends (all connected users) will
        recieve this json.  It is fed in this function so that the corresponding
        model in the collection can be updated (this update does NOT happen in this
        method)
    
        :param BaseModel item:
        :param string item['@type': The model type
        :rtype: BaseCollection
    .. js:function:: getDiscussionModelPromise()
    
        Returns the model of discussions
    
        :rtype: BaseModel
    .. js:function:: getDiscussionPreferencePromise()
    
        Returns the collection of preferences for all discussions
    
        :rtype: BaseCollection
    .. js:function:: getDiscussionPreferencePromise()
    
        Returns the collection of preferences for all discussions
    
        :rtype: BaseCollection
    .. js:function:: getDiscussionSourceCollectionPromise()
    
        Returns the collection of external sources for discussions
    
        :rtype: BaseCollection
    .. js:function:: getDiscussionSourceCollectionPromise2()
    
        Returns the collection of external sources for discussions
    
        :rtype: BaseCollection
    .. js:function:: getFacebookAccessTokensPromise()
    
        Returns the collection of tokens to access Facebook accounts
    
        :rtype: BaseCollection
    .. js:function:: getGroupSpecsCollectionPromise(viewsFactory, url_structure_promise, skip_group_state)
    
        Returns the stored configuration of groups and panels
    
        :param Object viewsFactory:
        :param String url_structure_promise:
        :param Boolean skip_group_state:
        :rtype: BaseCollection
    .. js:function:: getIdeaContentLinkCollectionOnMessage(messageModel)
    
        Creates a collection of IdeaContentLink for a message
    
        :param Object messageModel: The Backbone model of the message
        :return: The collection of ideaContentLinks
        :rtype: BaseCollection
    .. js:function:: getLocalRoleCollectionPromise()
    
        Returns the collection of roles (a set of permissions)
    
        :rtype: BaseCollection
    .. js:function:: getMessageFullModelPromise(id, messagesStructureCollectionPromise)
    
        This returns a promise to a SINGLE model.
        In practice, this model is a member of the proper collection, and requests to the server are optimised and batched together.
        Primarily used by messages to get the actual body and other information we do not want to eagerly preload.
    
        :param String id:
        :param Promise messagesStructureCollectionPromise: (optional) A promise containing a structure of messages (messages ids and how they are related to each other, but not necessarily their content), for which you want to download the message contents. Defaults to the general message structure collection promise (this.getAllMessageStructureCollectionPromise()), which contains all messages except deleted messages which removal does not break the structure. For example, if you want to show in the messageList the presence of all messages including all deleted messages, you need to set this parameter to a promise which contains the structure of absolutely all messages.
        :rtype: Promise
    .. js:function:: getMessageFullModelRequestWorker(collectionManager, messagesStructureCollectionPromise)
    
        This class is essentially a subprocess that receives requests for specific models and a specific viewdef and:
        - Combines them together to avoid swarming the server
        - Splits them to respect limits on http get url length
        - Dispaches the individual promises for each request even if they were actually processed together.
    
        :param Object collectionManager:
        :param Promise messagesStructureCollectionPromise: (optional) A promise containing a structure of messages (messages ids and how they are related to each other, but not necessarily their content), for which you want to download the message contents. Defaults to the general message structure collection promise (collectionManager.getAllMessageStructureCollectionPromise()), which contains all messages except deleted messages which removal does not break the structure. For example, if you want to show in the messageList the presence of all messages including all deleted messages, you need to set this parameter to a promise which contains the structure of absolutely all messages.
        :rtype: Promise
    .. js:function:: getMessageFullModelsPromise(ids)
    
        TODO: This method seems to not be used anywhere in the code. Remove it or use it. If we use it, add a second parameter messagesStructureCollectionPromise like in getMessageFullModelPromise().
        Retrieve fully populated models for the list of id's given
    
        :param Array.<string> ids: array of message id's
        :rtype: MessageModel
    .. js:function:: getNotificationsDiscussionCollectionPromise()
    
        Returns the collection of discussion notifications
    
        :rtype: BaseCollection
    .. js:function:: getUserLanguagePreferencesPromise()
    
        Returns the collection of language preferences
    
        :rtype: BaseCollection
    .. js:function:: getUserPreferencePromise()
    
        Returns the collection of preferences for all users
    
        :rtype: BaseCollection
    .. js:function:: getWidgetsForContextPromise(context, idea, liveupdate_keys)
    
        Returns a subset of widgets according to the context and the idea
    
        :param Number context:
        :param Object idea:
        :param Object liveupdate_keys:
        :rtype: BackboneSubset
    .. js:function:: getWidgetsForContextPromise()
    
        Returns the connected socket
    
        :rtype: Socket




