

context
=======

Useful global variables and methods.



.. currentmodule:: app.common


.. js:class:: Context ()

    
    
    .. js:function:: __createAnnotatorSelectionTooltipDiv()
    
        Creates the selection tooltip
    .. js:function:: _test_set_locale()
    .. js:function:: addUTCTimezoneToISO8601(e)
    
        [A utility function to convert backend DateTime data (ISO 8601 String) into ISO 8601 String with UTC Timezone]
        TODO: This function was taken from app/js/models/social.js. Refactor to use this Ctx version throughout codebase.
    
        :param string e:
        :return: ISO 8601 String with UTC Timezone
        :rtype: String
    .. js:function:: appendExtraURLParams(url, params)
    
        Helper function to add query string to a URL
    
        :param string url: The URL to append query string to
        :param Array.<object> params: An array of key-value objects denoting the query string, raw (unencoded)
        :return: The query string updated URL
        :rtype: string
    .. js:function:: canUseExpertInterface()
    
        Checks user permissions to use expert interface
    
        :rtype: Boolean
    .. js:function:: clearModal()
    
        Utility method to close the modal view properly
    .. js:function:: convertUrlsToLinks()
    .. js:function:: deanonymizationCifInUrl()
    .. js:function:: debug()
    .. js:function:: DEPRECATEDgetMessageListConfigFromStorage()
    
        Returns the Object with mesagelistconfig in the localStorage
    
        :rtype: Object
    .. js:function:: DEPRECATEDsetMessageListConfigToStorage(messageListConfig)
    
        Adds a panel in the localStorage
    
        :param Object messageListConfig: The Object with mesagelistconfig in the localStorage
        :return: The Object with mesagelistconfig in the localStorage
        :rtype: Object
    .. js:function:: escapeHtml(str)
    
        Use the browser's built-in functionality to quickly and safely escape the string
    
        :param String str: returns {String}
    .. js:function:: extractId(str)
    
        Given the string in the format "local:ModelName/{id}" returns the id
    
        :param string str:
        :rtype: string
    .. js:function:: format(string, arguments)
    
        Format string function
    
        :param string string:
        :param Array.<string> arguments:
        :rtype: string
    .. js:function:: formatAvatarUrl(userID[, size=44])
    
        Returns the avatar's url formatted with the given size
    
        :param number userID: The user's ID
        :param number size: The avatar size
        :rtype: string
    .. js:function:: formatDate(date, format)
    
        Format date
    
        :param Date|timestamp date:
        :param string format: app.dateFormat The format
        :rtype: string
    .. js:function:: getAbsoluteURLFromDiscussionRelativeURL(url)
    
        Returns an absolute url from a discussion relative url
    
        :param String url:
        :rtype: String
    .. js:function:: getAbsoluteURLFromRelativeURL(url)
    
        Returns an absolute url from a relative url
    
        :param String url:
        :rtype: String
    .. js:function:: getApiUrl(url)
    
        Formats the url to the current api url
    
        :param string url:
        :return: The url formatted
        :rtype: string
    .. js:function:: getApiV2DiscussionUrl(url)
    
        Formats the url to the current api v2  discussion url
    
        :param string url:
        :return: The url formatted
        :rtype: string
    .. js:function:: getApiV2Url(url)
    
        Formats the url to the current api v2 url
    
        :param string url:
        :return: The url formatted
        :rtype: string
    .. js:function:: getCookieItem()
    .. js:function:: getCsrfToken()
    
        Returns the CRSF token
    
        :rtype: String
    .. js:function:: getCurrentInterfaceType()
    
        Returns the user interface the user wants simple or expert
    
        :return: interface_id, one of SIMPLE, EXPERT
        :rtype: String
    .. js:function:: getCurrentSynthesisDraftPromise()
    
        Returns the draft of the current synthesis
    
        :rtype: Object
    .. js:function:: getCurrentTime()
    
        Return the current time
    
        :rtype: timestamp
    .. js:function:: getCurrentUser()
    
        Returns the connected user
    
        :rtype: Object
    .. js:function:: getCurrentUserFacebookAccountId()
    
        This assumes that the there is a 1:1 relationship
        between the AgentProfile (the user) and FacebookAccount
        the fbAccount if it exists, else returns undefined]
    
        :return: the @id of the account if any
        :rtype: String|undefined
    .. js:function:: getCurrentUserId()
    
        Returns the id of the connected user
    
        :rtype: String
    .. js:function:: getDiscussionId()
    
        Returns the id of the current discussion
    
        :rtype: String
    .. js:function:: getDiscussionSlug()
    
        Returns the slug of the discussion (name of discussion in the url)
    
        :rtype: String
    .. js:function:: getDraggedAnnotation()
    
        Returns the dragged annotation
    
        :rtype: String
    .. js:function:: getDraggedSegment()
    
        Returns the current segment
    
        :return: segment
        :rtype: String
    .. js:function:: getErrorMessageFromAjaxError(response)
    
        Returns an error message when an Ajax request fail
    
        :param Object response:
        :rtype: string
    .. js:function:: getJsonFromScriptTag(id)
    
        Returns embed JSON in the html.
    
        :param selector id:
        :rtype: Json
    .. js:function:: getLocale()
    
        Returns the current locale
    
        :rtype: String
    .. js:function:: getLocaleToLanguageNameCache()
    
        Cache of the locale to locale-name. The language names will be sent from the back-end in the language of the interface.
        Found from the <script id="translation-locale-names"></script>
        eg. Interface language: EN
        eg. {"fr": "French"}
        eg. Interface language FR
        eg. {"fr": "Francais"}
    
        :return: Language cache
        :rtype: object
    .. js:function:: getLoginURL()
    
        Returns the URL of the login page
    
        :rtype: String
    .. js:function:: getMessageViewStyleDefById(messageViewStyleId)
    
        get a view style definition by id
    
        :param string messageViewStyleId:
        :rtype: messageViewStyle
    .. js:function:: getNiceDate(date, precise, forbid_future)
    
        Returns a fancy date (ex: a few seconds ago) without time
    
        :param Date date:
        :param Boolean precise:
        :param  forbid_future:
        :rtype: string
    .. js:function:: getNiceDateTime(date, precise, with_time, forbid_future)
    
        Returns a fancy date (ex: a few seconds ago), or a formatted precise date if precise is true
    
        :param Date date:
        :param Boolean precise:
        :param Boolean with_time:
        :param  forbid_future:
        :rtype: string
    .. js:function:: getPermissionTokenPromise(json, id)
    
        Write Json in the html
    
        :param Json json:
        :param Selector id:
    .. js:function:: getPostIdFromAnnotation(annotation)
    
        Returns the Post related to the given annotation
    
        :param Annotation annotation:
        :rtype: Message
    .. js:function:: getPreferences()
    
        Returns discussion preferences.
    
        :rtype: Object
    .. js:function:: getReadableDateTime(date)
    
        Returns a nicely formatted date, but not an approximative expression (i.e. not "a few seconds ago")
    
        :param Date date:
        :rtype: string
    .. js:function:: getRelativeURLFromDiscussionRelativeURL(url)
    
        Returns an relative url from a discussion relative url
    
        :param String url:
        :rtype: String
    .. js:function:: getRoleNames()
    
        Returns names of roles
    
        :rtype: Array
    .. js:function:: getSocketUrl()
    
        Returns the URL of the socket
    
        :rtype: String
    .. js:function:: getTooltipsContainerSelector()
    .. js:function:: getTranslationServiceData()
    
        Cache of the translation service data stored in the <script id="translation-service-data"></script>
    
        :rtype: Json
    .. js:function:: getUrlFromUri(str)
    
        Returns an formatted url
    
        :param string str:
        :rtype: string
    .. js:function:: hasTranslationService()
    
        Checks if translation service is available
    
        :rtype: Boolean
    .. js:function:: htmlEntities(html)
    
        Convert all applicable characters to HTML entities
    
        :param string html: returns {String}
    .. js:function:: init()
    .. js:function:: initMomentJsLocale()
    
        Moment.j only has specific locales, for example, it has fr-ca, but no fr-fr. If you add new language support, you need to add it here.  Supported locales for moment.js can be found in /assembl/static/js/node_modules/moment/locale/
    .. js:function:: initTooltips(elm)
    
        init tool tips on each element with data-toggle attribute
    
        :param Selector elm:
    .. js:function:: isApplicationUnderProduction()
    
        :rtype: Boolean
    .. js:function:: isApplicationUnderTest()
    
        :rtype: Boolean
    .. js:function:: isElementInViewport()
    
        Checks if an element is in the viewport
    
        :rtype: Boolean
    .. js:function:: isInFullscreen()
    
        Checks if there is a panel in fullscreen mode ( i.e.: there is only one open )
    
        :rtype: boolean
    .. js:function:: isSmallScreen()
    
        Checks if user use a small screen
    
        :rtype: Boolean
    .. js:function:: isUserConnected()
    
        Checks if user is connected
    
        :rtype: Boolean
    .. js:function:: loadCsrfToken()
    
        fallback: synchronously load app.csrfToken
    
        :return: csrfToken
        :rtype: String
    .. js:function:: loadTemplate(id)
    
        Returns a template from an script tag
    
        :param string id: The id of the script tag
        :return: The Underscore.js _.template return
        :rtype: function
    .. js:function:: makeLinksShowOembedOnHover()
    .. js:function:: manageLastCurrentUser()
    .. js:function:: onAjaxError()
    .. js:function:: onDropdownClick()
    .. js:function:: openTargetInModal()
    
        Opens the clicked element with data attribute in modal
        Display options are retrieved from evt.currentTarget attributes or from the "options" parameter (Object used as an associative array).
        Modal can be dynamically resized once the iframe is loaded, or on demand.
    
        :rtype: Boolean
    .. js:function:: openTargetInPopOver()
    
        Opens the clicked element with data attribute in pop over
    
        :rtype: Boolean
    .. js:function:: popDraggedIdea()
    
        Returns the dragged idea
    
        :return: idea
        :rtype: Object
    .. js:function:: popoverAfterEmbed()
    
        "this" has to be the popover div: $("#popover-oembed")
    .. js:function:: removeCurrentlyDisplayedTooltips()
    
        Removes all tooltips from the screen. Without this, active tooltips (those currently displayed) will be left dangling if the trigger element is removed from the dom.
    .. js:function:: saveCurrentAnnotationAsExtract()
    
        Saves the current annotation if there is any
    .. js:function:: setApplicationUnderProduction()
    .. js:function:: setApplicationUnderTest()
    .. js:function:: setCsrfToken(token)
    
        Set the CRSF token
    
        :param string token:
    .. js:function:: setCurrentModalView()
    
        Utility method used to identify to Ctx the View object that is the modal. Useful for closing the model in any part of the code instead of the context of where the Modal was instantiated.
    .. js:function:: setCurrentSynthesisDraftId()
    
        Set the id of the synthesis draft
    .. js:function:: setCurrentUser(user)
    
        Set useful informations about the connected user for analytics
    
        :param objet user:
    .. js:function:: setDraggedAnnotation(annotation, annotatorEditor)
    
        Set the current annotation
    
        :param  annotation:
        :param  annotatorEditor:
    .. js:function:: setDraggedSegment(segment)
    
        Set the current segment
    
        :param  segment:
    .. js:function:: setInterfaceType(interface_id)
    
        Set the user interface the user wants
    
        :param String interface_id: one of SIMPLE, EXPERT
    .. js:function:: setLocale(locale)
    
        Set the locale in a cookie and reload page
    
        :param String locale: key
    .. js:function:: showDragbox(ev, text)
    
        Shows the dragbox when user starts dragging an element
        This method is designed to be called in a dragstart event listener.
    
        :param Event ev: The event object
        :param string text: The text to be shown in the .dragbox
    .. js:function:: showTargetBySegment(segment)
    
        Shows the segment source in the better way related to the source
        e.g.: If it is an email, opens it, if it is a webpage, open in another window ...
    
        :param Segment segment:
    .. js:function:: stripHtml(html)
    
        This removes (rather than escape) all html tags
    
        :param string html:
        :return: The new string without html tags
        :rtype: string
    .. js:function:: unescapeHtml(escapedStr)
    
        UNSAFE with unsafe strings; only use on previously-escaped ones!
    
        :param String escapedStr:
        :rtype: String
    .. js:function:: updateCurrentUser()
    
        Get from database up-to-date information about current logged-in user.
    .. js:function:: writeJsonToScriptTag(json, id)
    
        Write Json in the html
    
        :param Json json:
        :param Selector id:




