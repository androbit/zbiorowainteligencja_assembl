assembl.auth package
====================

Submodules
----------

.. toctree::

   assembl.auth.make_saml
   assembl.auth.password
   assembl.auth.social_auth
   assembl.auth.upgradable_session
   assembl.auth.util
   assembl.auth.wordpress

Module contents
---------------

.. automodule:: assembl.auth
    :members:
    :show-inheritance:
