

langstring
==========

Manage string translation



.. currentmodule:: app.models


.. js:class:: LangString ()

    Lang string model. A multilingual string, composed of many LangStringEntry
    Frontend model for :py:class:`assembl.models.langstrings.LangString`

    :extends: :js:class:`app.models.base.BaseModel`
    



.. js:class:: LangStringCollection ()

    Lang string collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: LangStringEntry ()

    Lang string entry Model. A string in a given language. Many of those form a LangString
    Frontend model for :py:class:`assembl.models.langstrings.LangStringEntry`

    :extends: :js:class:`app.models.base.BaseModel`
    



.. js:class:: LangStringEntryCollection ()

    Lang string entry collection

    :extends: :js:class:`app.models.base.BaseCollection`
    



.. js:class:: LocaleUtils ()

    
    
    .. js:function:: getServiceShowOriginalString()
    .. js:function:: getServiceShowOriginalUrl()
    .. js:function:: localeAsTranslationService()
    .. js:function:: localeCompatibility()
    .. js:function:: stripCountry()
    .. js:function:: superLocale()




